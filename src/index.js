import React from "react";
import { hydrate, render } from "react-dom";
import "./index.css";
import App from "./App";
import TagManager from "react-gtm-module";

const tagManagerArgs = {
  gtmId: "GTM-PDR72PR"
};

TagManager.initialize(tagManagerArgs);

const rootElement = document.getElementById("root");
if (rootElement.hasChildNodes()) {
  hydrate(<App />, rootElement);
} else {
  render(<App />, rootElement);
}
