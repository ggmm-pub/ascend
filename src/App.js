import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Footer from "./components/Footer";
import Home from "./components/Home";
import Contact from "./components/Contact";
import About from "./components/About";
import SectionService from "./components/SectionService";
import Research from "./components/Research";
import Post from "./components/Post";
import Services from "./components/Services";
import VideoLanding from "./components/VideoLanding";
import { NavBar } from "ggmm-react-lib";
import axios from "axios";
import _ from "lodash";
import { CSSTransition, TransitionGroup } from "react-transition-group";

class App extends Component {
  constructor() {
    super();
    this.state = {
      siteInfo: "Ascend Talent Strategies",
      header: [],
      global: [],
      nav: []
    };
  }

  componentDidMount() {
    const SITE_URL = "https://dashboard.ascendtalentstrategies.com";

    axios.get(SITE_URL + "/wp-json/acf/v3/sitewide").then(res => {
      this.setState({
        global: res.data[0].acf
      });
    });

    //copy this to get data
    axios.get(SITE_URL + "/wp-json/menus/v1/menus/menu-1").then(res => {
      let navData = []; // set empty array

      // loop through data
      _.map(res.data.items, item => {
        //menu item name
        const name = item.post_title,
          link = item.url,
          set = {
            name: name, // name: 'About'
            link: link // link: /about
          };
        navData.push(set);
      });
      this.setState({
        nav: navData
      });
    });
  }

  render() {
    const global = this.state.global;
    return (
      <BrowserRouter>
        <div>
          <link
            rel="stylesheet"
            href="https://pro.fontawesome.com/releases/v5.1.0/css/all.css"
            integrity="sha384-87DrmpqHRiY8hPLIr7ByqhPIywuSsjuQAfMXAE0sMUpY3BM7nXjf+mLIUSvhDArs"
            crossOrigin="anonymous"
          />
          <div className="fixed-nav">
            <div className="pre-nav">
              <div className="pre-wrap">
                <div className="link">
                  <a href="tel:14148884287">414.888.4287</a>
                  <a href="https://www.linkedin.com/company/ascend-talent-strategies">
                    <i className="fab fa-linkedin" />
                  </a>
                  <a href="https://www.facebook.com/ascendtalentstrategies/">
                    <i className="fab fa-facebook-square" />
                  </a>
                </div>
              </div>
            </div>
            <NavBar
              backgroundColor="rgb(5, 28, 204)"
              mobileColor="rgb(5, 28, 204)"
              type="left"
              logo={global.logo}
              logoClass="navlogo"
              iconColor="gray"
              customClass="nav"
              height="90px"
              nav={this.state.nav}
            />
          </div>
          <Route
            render={({ location }) => (
              <TransitionGroup>
                <CSSTransition
                  key={location.key}
                  timeout={300}
                  classNames="fade"
                >
                  <Switch location={location}>
                    <Route
                      exact
                      path="/"
                      render={props => (
                        <Home siteTitle={this.state.siteInfo} {...props} />
                      )}
                    />
                    <Route
                      exact
                      path="/about"
                      render={props => (
                        <About siteTitle={this.state.siteInfo} {...props} />
                      )}
                    />
                    <Route
                      path="/introducing-ascend"
                      render={props => (
                        <VideoLanding
                          siteTitle={this.state.siteInfo}
                          {...props}
                        />
                      )}
                    />
                    <Route
                      exact
                      path="/contact"
                      render={props => (
                        <Contact siteTitle={this.state.siteInfo} {...props} />
                      )}
                    />
                    <Route
                      exact
                      path="/services"
                      render={props => (
                        <SectionService
                          siteTitle={this.state.siteInfo}
                          {...props}
                        />
                      )}
                    />
                    <Route
                      exact
                      path="/research-and-insights"
                      render={props => (
                        <Research siteTitle={this.state.siteInfo} {...props} />
                      )}
                    />
                    <Route
                      exact
                      path="/post/:id"
                      render={props => (
                        <Post siteTitle={this.state.siteInfo} {...props} />
                      )}
                    />
                    <Route
                      exact
                      path="/services/:id"
                      render={props => (
                        <Services siteTitle={this.state.siteInfo} {...props} />
                      )}
                    />
                  </Switch>
                </CSSTransition>
              </TransitionGroup>
            )}
          />
          <link
            rel="stylesheet"
            href="https://pro.fontawesome.com/releases/v5.7.1/css/all.css"
            integrity="sha384-6jHF7Z3XI3fF4XZixAuSu0gGKrXwoX/w3uFPxC56OtjChio7wtTGJWRW53Nhx6Ev"
            crossOrigin="anonymous"
          />

          <link
            href="https://fonts.googleapis.com/css?family=Podkova:700|Tajawal:500,800"
            rel="stylesheet"
          />
          <Footer title={this.state.siteInfo} />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
