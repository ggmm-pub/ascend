import React, { Component } from "react";

export default class SVGarrow extends Component {
  render() {
    return (
      <div>
        <svg
          className="arrow"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 289 321"
        >
          <path d="M-361.9 104.4c1.9 0 1.9-3 0-3s-1.9 3 0 3z" />
          <path d="M4.8 176.9l139.7 138 139.7-138-33.3-31.1-83.8 83.1V6.1l-45.2.1v222.7l-83.1-81.5z" />
        </svg>
      </div>
    );
  }
}
