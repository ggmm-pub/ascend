import React, { Component } from "react";
import axios from "axios";
import JotformEmbed from "react-jotform-embed";

class SectionContact extends Component {
  constructor() {
    super();
    this.state = {
      contact: []
    };
  }

  componentDidMount() {
    const SITE_URL = "https://dashboard.ascendtalentstrategies.com";

    axios.get(SITE_URL + "/wp-json/wp/v2/pages?slug=contact").then(res => {
      this.setState({
        contact: res.data[0],
        jotformSource: res.data[0].acf.jotform_source,
        pageContent: res.data[0].content.rendered
      });
    });
  }
  render() {
    const { pageContent, jotformSource } = this.state;

    return (
      <div className="contact-flex">
        <div className="contact-info">
          <div
            className="content"
            dangerouslySetInnerHTML={{ __html: pageContent }}
          />
        </div>
        <div className="c-form">
          {jotformSource && <JotformEmbed src={jotformSource} />}
        </div>
      </div>
    );
  }
}

export default SectionContact;
