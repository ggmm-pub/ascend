import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

import _ from "lodash";
export default class PostBrowser extends Component {
  constructor() {
    super();
    this.state = {
      activeTag: "All"
    };
  }
  componentDidMount() {
    window.scrollTo(0, 0);
    const SITE_URL = "https://dashboard.ascendtalentstrategies.com";

    axios.get(SITE_URL + "/wp-json/wp/v2/tags").then(res => {
      this.setState({
        tags: res.data
      });
    });

    axios.get(SITE_URL + "/wp-json/wp/v2/posts?_embed").then(res => {
      this.setState({
        data: res.data
      });
    });
  }
  handleClick = name => e => {
    this.setState({
      activeTag: name
    });
  };

  renderPosts() {
    const { data, activeTag } = this.state;
    return _.map(data, (o, i) => {
      return _.map(o.acf.tags, tag => {
        if (i > 2 && tag.name === activeTag) {
          return (
            <Link
              to={"/post/" + o.slug}
              className="simple-post"
              style={{
                backgroundImage:
                  "url(" + o.better_featured_image.source_url + ")"
              }}
            >
              <div>
                <div className="small-button yellow-button">Read Post</div>
                <div className="top-box">
                  <h1>{o.title.rendered}</h1>
                  <h2>{o.acf.subheadline}</h2>
                </div>
              </div>
            </Link>
          );
        }
      });
    });
  }

  renderTags() {
    if (this.state) {
      const { tags } = this.state;
      return _.map(tags, o => {
        return (
          <div
            onClick={this.handleClick(o.name)}
            className={
              this.state.activeTag === o.name ? "tag-btn active" : "tag-btn"
            }
          >
            <h4>{o.name}</h4>
          </div>
        );
      });
    }
  }
  render() {
    return (
      <div className="post-browser">
        <div className="tag-control">{this.renderTags()}</div>
        <div className="divider yellow-back" />
        <div className="post-flex">{this.renderPosts()}</div>
      </div>
    );
  }
}
