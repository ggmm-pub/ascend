import React, { Component } from "react";
import axios from "axios";
import BlogHero from "./BlogHero";
import Head from "./Head";
import SectionOtherPosts from "./SectionOtherPosts";

export default class Post extends Component {
  componentDidMount() {
    const SITE_URL = "https://dashboard.ascendtalentstrategies.com",
      { id } = this.props.match.params;

    axios
      .get(SITE_URL + "/wp-json/wp/v2/posts?slug=" + id + "&_embed")
      .then(res => {
        this.setState(
          {
            data: res.data[0],
            seo: res.data[0].yoast_meta
          },
          () => window.scrollTo(0, 0)
        );
      });
  }

  renderHead() {
    const { seo, data } = this.state,
      url = window.location.href;

    if (seo) {
      return (
        <Head
          ogImage={data.better_featured_image.source_url}
          pageTitle={seo.yoast_wpseo_title}
          url={url}
          des={seo.yoast_wpseo_metadesc}
        />
      );
    }
  }

  render() {
    if (this.state) {
      const { data } = this.state;
      return (
        <div className="post">
          {this.renderHead()}
          <BlogHero excerpt={true} featuredPost={data} />
          <div
            className="content res-cont"
            dangerouslySetInnerHTML={{
              __html: data.content.rendered
            }}
          />

          <SectionOtherPosts id={data.id} />
        </div>
      );
    } else {
      return false;
    }
  }
}
