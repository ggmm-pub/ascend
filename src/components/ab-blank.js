import React, { Component } from "react";

import _ from "lodash";
import { app } from "../services/flamelink";

export default class Blank2 extends Component {
  constructor() {
    super();
    this.state = {
      grid: []
    };
  }

  componentDidMount() {
    app.content
      .get(this.props.grid, {
        populate: ["logoImage"]
      })
      .then(grid =>
        this.setState({
          grid: grid
        })
      );
  }

  render() {
    return <div />;
  }
}
