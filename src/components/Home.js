import React, { Component } from "react";
import Head from "./Head";
import axios from "axios";
import _ from "lodash";
import { Hero, Grid } from "ggmm-react-lib";
import { Link } from "react-router-dom";
import SectionService from "./SectionService";
import SectionReviews from "./SectionReviews";

import SectionPostFeat from "./SectionPostFeat";

class Home extends Component {
  constructor() {
    super();
    this.state = {
      currentReview: 0
    };
  }

  componentDidMount() {
    const SITE_URL = "https://dashboard.ascendtalentstrategies.com";

    axios.get(SITE_URL + "/wp-json/wp/v2/pages?slug=homepage").then(res => {
      let logoData = [];

      // loop through data
      _.map(res.data[0].acf.logo_grid, item => {
        // item name
        const image = item.company_logo,
          set = {
            image: image
          };
        logoData.push(set);
      });

      this.setState(
        {
          seo: res.data[0].yoast_meta,
          homepageData: res.data[0].acf,
          logo: logoData
        },
        () => window.scrollTo(0, 0)
      );
    });
  }

  renderContent() {
    const data = this.state.homepageData;
    if (data) {
      return (
        <div
          className="home-hero"
          style={{
            backgroundImage: "url(" + data.background_image + ")"
          }}
        >
        <a className="banner-overlay" href={data.overlay_link} target="_blank">
          <img
            src={data.banner_overlay}
            alt={data.overlay_alt}
          />
        </a>
          <div className="hero-content">
            <div className="content-container">
              <div className="hero-flex">
                <img
                  className="slogan"
                  src={data.slogan_svg}
                  alt={data.main_slogan}
                />
                <Link className="main-button" to={data.main_cta_link}>
                  {data.main_cta}
                </Link>
              </div>
              <div className="home-divider" />
            </div>
          </div>
          <Hero
            pageTitle="Home"
            type="video"
            videoId={data.video_id}
            height="70"
            customClass="hero-home-video"
          />
        </div>
      );
    }
  }

  renderHead() {
    const { seo, homepageData } = this.state,
      url = window.location.href;

    if (seo) {
      return (
        <Head
          ogImage={homepageData.background_image}
          pageTitle={seo.yoast_wpseo_title}
          url={url}
          des={seo.yoast_wpseo_metadesc}
        />
      );
    }
  }

  renderbody() {
    const data = this.state.homepageData;
    if (data) {
      return (
        <div className="about-section">
          <div className="about-block">
            <div
              className="about-copy"
              dangerouslySetInnerHTML={{ __html: data.about_copy }}
            />
            <Link className="small-button" to={data.about_button_link}>
              {data.about_button_cta}
            </Link>
          </div>
          <div className="about-logo">
            <div className="logo-wrap">
              <h4>{data.logo_block_header}</h4>
              <Grid
                gridGap="0px"
                columns="4"
                tabletColumns="2"
                mobileColumns="2"
                type="icon"
                data={this.state.logo}
              />
            </div>
          </div>
          <div className="home-video">
            <div class="embed-container">
              <iframe
                title="Home Video"
                src={data.feature_video}
                frameborder="0"
                webkitAllowFullScreen
                mozallowfullscreen
                allowFullScreen
              />
            </div>
          </div>
          <SectionService />
          <SectionPostFeat />
        </div>
      );
    }
  }

  render() {
    const { currentReview, homepageData } = this.state;

    return (
      <div>
        {this.renderHead()}
        {this.renderContent()}
        {this.renderbody()}
        {homepageData && <SectionReviews data={homepageData} />}
      </div>
    );
  }
}

export default Home;
