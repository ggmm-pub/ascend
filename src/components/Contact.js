import React, { Component } from "react";
import axios from "axios";
import SectionContact from "./SectionContact";
import Head from "./Head";

class Contact extends Component {
  constructor() {
    super();
    this.state = {
      contact: []
    };
  }

  componentDidMount() {
    const SITE_URL = "https://dashboard.ascendtalentstrategies.com";

    axios.get(SITE_URL + "/wp-json/wp/v2/pages?slug=contact").then(res => {
      this.setState(
        {
          blockContent: res.data[0].acf.parking_information,
          seo: res.data[0].yoast_meta,
          data: res.data[0]
        },
        () => window.scrollTo(0, 0)
      );
    });
  }

  renderHead() {
    const { seo, data } = this.state,
      url = window.location.href;

    if (seo) {
      return (
        <Head
          ogImage={data.better_featured_image.source_url}
          pageTitle={seo.yoast_wpseo_title}
          url={url}
          des={seo.yoast_wpseo_metadesc}
        />
      );
    }
  }
  render() {
    const { blockContent } = this.state;

    return (
      <div className="contact-wrap contact-page">
        {this.renderHead()}
        <SectionContact />
        <div
          className="contact-info-block"
          dangerouslySetInnerHTML={{ __html: blockContent }}
        />
      </div>
    );
  }
}

export default Contact;
