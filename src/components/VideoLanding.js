import React, { Component } from "react";
import axios from "axios";
import Contact from "./Contact";
export default class VideoLanding extends Component {
  constructor() {
    super();
    this.state = {
      homepageData: []
    };
  }
  componentDidMount() {
    const SITE_URL = "https://dashboard.ascendtalentstrategies.com";

    axios.get(SITE_URL + "/wp-json/wp/v2/pages?slug=homepage").then(res => {
      this.setState(
        {
          homepageData: res.data[0].acf
        },
        () => window.scrollTo(0, 0)
      );
    });
  }

  renderVideo() {
    const { homepageData } = this.state;
    if (homepageData) {
      return (
        <div class="embed-container">
          <iframe
            title="Home Video"
            src={homepageData.feature_video}
            frameborder="0"
            webkitAllowFullScreen
            mozallowfullscreen
            allowFullScreen
          />
        </div>
      );
    }
  }

  render() {
    return (
      <div className="intro-ascend">
        <div className="home-video">{this.renderVideo()}</div>
        <Contact />
      </div>
    );
  }
}
