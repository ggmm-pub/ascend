import React, { Component } from "react";
import Head from "./Head";
import axios from "axios";
import _ from "lodash";
import { Link } from "react-router-dom";

class SectionService extends Component {
  constructor() {
    super();
    this.state = {
      services: null
    };
  }

  componentDidMount() {
    const SITE_URL = "https://dashboard.ascendtalentstrategies.com";

    axios.get(SITE_URL + "/wp-json/wp/v2/services").then(res => {
      this.setState({
        services: res.data
      });
    });

    axios
      .get(SITE_URL + "/wp-json/wp/v2/pages?slug=services")
      .then(res => {
        this.setState(
          {
            image: res.data[0].better_featured_image,
            pageTitle: res.data[0].title.rendered,
            content: res.data[0].content.rendered,
            seo: res.data[0].yoast_meta
          },
          () => window.scrollTo(0, 0)
        );
      })
      .catch(error => console.log(error));
  }

  renderdesc() {
    return (
      <div className="service-desc">
        <h1>{this.state.pageTitle}</h1>
        <div dangerouslySetInnerHTML={{ __html: this.state.content }} />
      </div>
    );
  }

  renderposts() {
    if (!this.state.services) {
      return <div>Loading...</div>;
    } else {
      return _.map(this.state.services, (post, i) => {
        return (
          <div key={i} className="service-block">
            <Link to={`/services/${post.slug}`}>
              <img alt={post.title.rendered} src={post.acf.service_icon} />
              <h2>{post.title.rendered}</h2>
            </Link>
            <div className="more-info">
              <ul>
                {_.map(post.acf.sblock_bull, bullets => {
                  return <li>{bullets.subservices}</li>;
                })}
              </ul>
            </div>

            <Link to={`/services/${post.slug}`} className="more-button">
              <h4>Learn More</h4>
            </Link>
          </div>
        );
      });
    }
  }

  renderHead() {
    const { seo, image } = this.state,
      url = window.location.href;

    if (seo) {
      return (
        <Head
          ogImage={image.source_url}
          pageTitle={seo.yoast_wpseo_title}
          url={url}
          des={seo.yoast_wpseo_metadesc}
        />
      );
    }
  }

  render() {
    return (
      <div className="services-sec">
        {this.renderHead()}
        <div className="service-flex">
          {this.renderdesc()}
          {this.renderposts()}
        </div>
      </div>
    );
  }
}

export default SectionService;
