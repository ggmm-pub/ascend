import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import _ from "lodash";

export default class SectionOtherPosts extends Component {
  constructor() {
    super();
    this.state = {};
  }
  componentDidMount() {
    const SITE_URL = "https://dashboard.ascendtalentstrategies.com";

    axios.get(SITE_URL + "/wp-json/wp/v2/posts?_embed").then(res => {
      this.setState({
        featuredPost: res.data[0],
        posts: res.data
      });
    });
  }

  renderRelatedPosts() {
    const { posts } = this.state,
      { id } = this.props,
      currentId = _.findIndex(posts, { id: id });
    let newArr = [];

    if (posts) {
      // eslint-disable-next-line
      const randomPosts = _.map(posts, (o, i) => {
        if (i !== currentId) {
          newArr.push(o);
        }
      });
    }

    return _.map(newArr, (o, i) => {
      if (i <= 2) {
        return (
          <Link
            to={"/post/" + o.slug}
            key={i}
            className="simple-post"
            style={{
              backgroundImage: "url(" + o.better_featured_image.source_url + ")"
            }}
          >
            <div>
              <div className="small-button yellow-button">Read Post</div>
              <div className="top-box">
                <h1>{o.title.rendered}</h1>
                <h2>{o.acf.subheadline}</h2>
              </div>
            </div>
          </Link>
        );
      }
    });
  }

  render() {
    return (
      <div className="related-post">
        <div className="div-wrap">
          <div className="divider yellow-back " />
          <h1>more posts</h1>
        </div>
        <div className="post-flex">{this.renderRelatedPosts()}</div>
      </div>
    );
  }
}
