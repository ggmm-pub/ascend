import React, { Component } from "react";
import axios from "axios";
import BlogFeatHero from "./BlogFeatHero";
import Head from "./Head";
import PostBrowser from "./PostBrowser";
import _ from "lodash";
import { Link } from "react-router-dom";

export default class Research extends Component {
  constructor() {
    super();
    this.state = {};
  }
  componentDidMount() {
    const SITE_URL = "https://dashboard.ascendtalentstrategies.com";

    axios.get(SITE_URL + "/wp-json/wp/v2/posts?_embed").then(res => {
      this.setState(
        {
          featuredPost: res.data[0],
          seo: res.data[0].yoast_meta,
          posts: res.data,
          author: res.data[0].acf.author
        },
        () => window.scrollTo(0, 0)
      );
    });
  }

  renderSecondaryPosts() {
    const { posts, featuredPost } = this.state;
    return _.map(posts, (o, i) => {
      if (i !== 0 && i <= 2) {
        return (
          <Link
            to={"/post/" + o.slug}
            className="secondary-blog-post"
            style={{
              backgroundImage: "url(" + o.better_featured_image.source_url + ")"
            }}>
            <div className="top-box">
              <h1>{o.title.rendered}</h1>
              <h2>{o.acf.subheadline}</h2>
              <h4 className="author">by {o.acf.author}</h4>
            </div>

            <div className="bottom-box">
              <div
                className="post-excerpt"
                dangerouslySetInnerHTML={{
                  __html: o.excerpt.rendered
                }}
              />
              <div className="small-button">Read Post</div>
            </div>
          </Link>
        );
      }
    });
  }

  renderHead() {
    const { seo, posts } = this.state,
      url = window.location.href;

    if (seo) {
      return (
        <Head
          ogImage={posts[0].better_featured_image.source_url}
          pageTitle={seo.yoast_wpseo_title}
          url={url}
          des={seo.yoast_wpseo_metadesc}
        />
      );
    }
  }

  render() {
    const { featuredPost, posts } = this.state;
    return (
      <div className="research">
        {this.renderHead()}
        <BlogFeatHero excerpt={true} featuredPost={featuredPost} />
        <div className="post-wrap">{this.renderSecondaryPosts()}</div>
        <PostBrowser posts={posts} />
      </div>
    );
  }
}
