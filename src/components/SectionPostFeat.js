import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import _ from "lodash";

export default class SectionPostFeat extends Component {
  constructor() {
    super();
    this.state = {};
  }
  componentDidMount() {
    const SITE_URL = "https://dashboard.ascendtalentstrategies.com";

    axios.get(SITE_URL + "/wp-json/wp/v2/posts?_embed").then(res => {
      this.setState({
        featuredPost: res.data[0],
        posts: res.data
      });
    });
  }

  renderSecondaryPosts() {
    const { posts } = this.state;
    return _.map(posts, (o, i) => {
      if (i !== -1 && i <= 0) {
        return (
          <div className="home-blog-feat">
            <div
              className="feat-img"
              style={{
                backgroundImage:
                  "url(" + o.better_featured_image.source_url + ")"
              }}
            />
            <div className="title-box">
              <h1>{o.title.rendered}</h1>
              <h2>{o.acf.subheadline}</h2>
              <h4 className="author">by {o.acf.author}</h4>
            </div>

            <div className="post-preview">
              <div
                className="post-excerpt"
                dangerouslySetInnerHTML={{
                  __html: o.excerpt.rendered
                }}
              />
              <Link to={"/post/" + o.slug} className="small-button">
                Read Post
              </Link>
            </div>
          </div>
        );
      }
    });
  }

  render() {
    return <div className="research">{this.renderSecondaryPosts()}</div>;
  }
}
