import React, { Component } from "react";
import axios from "axios";
import Mailchimp from "react-mailchimp-form";
import mmac from '../assets/MMAC.svg';
import { Link } from "react-router-dom";

export default class Footer extends Component {
  constructor() {
    super();
    this.state = {};
  }

  componentDidMount() {
    const SITE_URL = "https://dashboard.ascendtalentstrategies.com";

    axios.get(SITE_URL + "/wp-json/acf/v3/sitewide").then(res => {
      this.setState({
        global: res.data[0].acf
      });
    });
  }

  render() {
    const global = this.state.global;

    return (
      <footer className="footer">
        {global && (
     <div>
     <div className="foot-flex">
            <div className="newsletter">
              <h2>{global.newsletter_header}</h2>
              <Mailchimp
                className="mc-form"
                action={global.mailchimp}
                fields={[
                  {
                    name: "EMAIL",
                    placeholder: "email",
                    type: "email",
                    required: true
                  }
                ]}
              />
            </div>
       
            <div className="mmac-logo">
              <img alt="MMAC" src={mmac} />
            </div>
            <div className="foot-butt">
              <Link className="main-button" to={global.button_link}>
                {global.button_cta}
              </Link>
            </div>
          </div>
          <div className="foot-logo">
              <img alt="Ascend" src={global.footer_logo} />
              <p>© {new Date().getFullYear()} Ascend Talent Strategies</p>
            </div>
     </div>
        )}
      </footer>
    );
  }
}


