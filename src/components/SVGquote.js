import React, { Component } from "react";

export default class SVGquote extends Component {
  render() {
    return (
      <div className="quote-cont">
        <svg
          className="quote"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 204 97"
        >
          <path d="M40.4 12.2c.2 0-.5 1-1.9 3s-3.1 4.5-5.1 7.6c-1.9 3-3.6 6-4.9 8.8 3 .8 5.7 2.2 8 4.2 2.3 1.9 4 4.1 5.3 6.6 1.3 2.4 1.9 4.8 1.9 7.2 0 5.9-1.9 10.7-5.7 14.3-3.8 3.6-8.6 5.4-14.5 5.4-5.4 0-10.2-2.1-14.3-6.2C5.1 59 3 54.1 3 48.4 3 43.1 4.2 38 6.5 33.1c2.4-5 5.2-9.6 8.6-13.8 3.4-4.2 6.6-7.8 9.6-10.7 3-2.9 5.1-5 6.3-6.2l9.4 9.8zm52.1 0c.2 0-.5 1-2 3s-3.2 4.5-5.2 7.6c-1.9 3-3.6 6-4.9 8.8 3 .8 5.7 2.2 8 4.2 2.3 1.9 4.1 4.1 5.4 6.6 1.3 2.4 2 4.8 2 7.2 0 5.9-1.9 10.7-5.7 14.3-3.8 3.6-8.7 5.4-14.8 5.4-5.2 0-9.9-2.1-14.2-6.2-4.2-4.1-6.3-9.1-6.3-14.8 0-5.2 1.2-10.3 3.5-15.3 2.4-5 5.2-9.6 8.6-13.8 3.4-4.2 6.6-7.8 9.6-10.7 3-2.9 5.2-5 6.6-6.2l9.4 9.9zM111.3 84.5c0-.2.8-1.2 2.3-3.2 1.5-1.9 3.2-4.4 5.2-7.5 1.9-3 3.5-6 4.7-8.8-2.9-.8-5.5-2.2-7.8-4.2-2.4-1.9-4.2-4.1-5.4-6.6-1.3-2.4-1.9-4.8-1.9-7.2 0-5.9 1.9-10.6 5.8-14.2 3.9-3.5 8.7-5.3 14.4-5.3 5.4 0 10.2 2 14.4 6.1 4.2 4 6.3 8.9 6.3 14.7 0 5.7-1.2 11.1-3.7 16-2.4 5-5.4 9.5-8.7 13.6-3.4 4.1-6.6 7.6-9.6 10.4-3 2.8-5.2 4.8-6.6 6.2l-9.4-10zm52.3 0c0-.2.7-1.2 2-3.2 1.3-1.9 3-4.4 5.1-7.5s3.6-6 4.8-8.8c-3-.8-5.7-2.2-8-4.2-2.3-1.9-4-4.1-5.3-6.6-1.3-2.4-1.9-4.8-1.9-7.2 0-5.9 1.9-10.6 5.7-14.2 3.8-3.5 8.6-5.3 14.5-5.3 5.2 0 9.9 2 14.2 6.1 4.2 4 6.3 8.9 6.3 14.7 0 5.4-1.2 10.6-3.5 15.5-2.4 5-5.2 9.6-8.6 13.8-3.4 4.2-6.6 7.8-9.6 10.6-3 2.9-5.1 5-6.3 6.3l-9.4-10z" />
        </svg>
      </div>
    );
  }
}
