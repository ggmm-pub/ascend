import React, { Component } from "react";
import Head from "./Head";
import axios from "axios";
import { Link } from "react-router-dom";
import _ from "lodash";

const SITE_URL = "https://dashboard.ascendtalentstrategies.com";

export default class PostService extends Component {
  constructor() {
    super();
    this.state = {
      post: [],
      global: [],
      pageTitle: "",
      nextPost: [],
      prevPost: []
    };
  }

  componentDidMount() {
    axios.get(SITE_URL + "/wp-json/wp/v2/services").then(res => {
      this.setState(
        {
          pageTitle: res.data[0].title.rendered,
          pageContent: res.data[0].content.rendered,
          post: res.data[0].acf,
          currentId: res.data[0].id,
          uploadedFile: res.data[0].acf.file_upload
        },
        () => window.scrollTo(0, 0)
      );
    });

    axios.get(SITE_URL + "/wp-json/wp/v2/services").then(res => {
      _.map(res.data, (o, index) => {
        const pLength = Object.keys(res.data).length - 1, //6
          currentIndex = this.state.currentId,
          currentPost = _.findIndex(res.data, ["id", currentIndex]);
        if (index === pLength && currentPost === 0) {
          this.setState({
            prevPost: o,
            prevTitle: o.title.rendered
          });
        } else if (index === currentPost + 1) {
          this.setState({
            nextPost: o,
            nextTitle: o.title.rendered
          });
        } else if (index === currentPost - 1 && currentPost !== 0) {
          this.setState({
            prevPost: o,
            prevTitle: o.title.rendered
          });
        } else if (index === 0 && currentPost === pLength) {
          this.setState({
            nextPost: o,
            nextTitle: o.title.rendered
          });
        }
      });
      // this.setState({
      //   allPosts: res.data
      // });
    });
  }

  renderHead() {
    const data = this.state.post,
      url = window.location.href;

    if (data) {
      return (
        <Head
          pageTitle={this.state.pageTitle}
          url={url}
          des="Click to view this entire blog post"
        />
      );
    }
  }

  render() {
    const path = this.props.match.path,
      remove = path.replace("/", ""),
      type = remove.replace(/\/.*/, ""),
      data = this.state.post;
    return (
      <div className="post-main">
        {this.renderHead()}
        <div className="grey-top" />
        <div className="sec-wrap">
          <div className="post-header">
            <div
              className="post-image"
              style={{ backgroundImage: "url(" + data.resource_image + ")" }}
            >
              <div className="p-title">
                <h1>{this.state.pageTitle}</h1>
                <div className="line" />
              </div>
            </div>
          </div>
          <div className="post-content">
            <div dangerouslySetInnerHTML={{ __html: this.state.pageContent }} />
            {this.state.uploadedFile && (
              <a
                href={this.state.uploadedFile}
                target="_blank"
                rel="noopener noreferrer"
              >
                <i className="fal fa-file-chart-pie" />
                {data.rbutt_cta}
              </a>
            )}
          </div>
          <div className="post-nav">
            <div className="flex-container">
              <Link
                className="main-button grey-butt"
                to={"/" + type + "/" + this.state.prevPost.slug}
              >
                {this.state.prevTitle}
              </Link>
              <Link
                className="main-button grey-butt"
                to={"/" + type + "/" + this.state.nextPost.slug}
              >
                {this.state.nextTitle}
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
