import React, { Component } from "react";
import { Hero, Loader } from "ggmm-react-lib";
import { Link } from "react-router-dom";

export default class BlogFeatHero extends Component {
  render() {
    const { featuredPost } = this.props;

    if (featuredPost) {
      const timestamp = featuredPost.date,
        year = timestamp.substring(0, 4),
        postMonth = timestamp.substring(5, 7),
        monthIndex = +postMonth.replace(/^0+/, "") - +1,
        months = [
          "January",
          "February",
          "March",
          "April",
          "May",
          "June",
          "July",
          "August",
          "September",
          "October",
          "November",
          "December"
        ];

      return (
        <div>
          <Hero
            customClass="research-hero"
            imageClass="res-image"
            type="image"
            imageUrl={featuredPost.better_featured_image.source_url}
            hasChildren={true}
            bgPosition="center center"
          >
            {" "}
            <div className="date">{months[monthIndex] + " " + year}</div>
            <h1 className="res-title">
              research
              <br />& insights
            </h1>
          </Hero>

          <div className="hero-info">
            <div className="blog-box">
              <div className="top-box">
                <h1>{featuredPost.title.rendered}</h1>
                <h2>{featuredPost.acf.subheadline}</h2>
                <h4 className="author">by {featuredPost.acf.author}</h4>
              </div>
              {this.props.excerpt && (
                <div className="bottom-box">
                  <div
                    className="post-excerpt"
                    dangerouslySetInnerHTML={{
                      __html: featuredPost.excerpt.rendered
                    }}
                  />
                  <Link
                    to={"/post/" + featuredPost.slug}
                    className="small-button"
                  >
                    Read Post
                  </Link>
                </div>
              )}
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <Loader
          title="Loading"
          spinnerColor="#969696"
          fullHeight={true}
          backgroundColor="#e2e2e2"
        />
      );
    }
  }
}
