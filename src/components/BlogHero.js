import React, { Component } from "react";
import { Hero, Loader } from "ggmm-react-lib";
import SVGarrow from "./SVGarrow";

export default class BlogHero extends Component {
  render() {
    const { featuredPost } = this.props;

    if (featuredPost) {
      const timestamp = featuredPost.date,
        year = timestamp.substring(0, 4),
        postMonth = timestamp.substring(5, 7),
        monthIndex = +postMonth.replace(/^0+/, "") - +1,
        months = [
          "January",
          "February",
          "March",
          "April",
          "May",
          "June",
          "July",
          "August",
          "September",
          "October",
          "November",
          "December"
        ];

      return (
        <Hero
          customClass="single-hero"
          type="image"
          imageUrl={featuredPost.better_featured_image.source_url}
          height="50"
          minHeight="250"
          hasChildren={true}
          bgPosition="center center"
        >
          <div className="single-info">
            <div className="date">{months[monthIndex] + " " + year}</div>
            <div className="title-box">
              <h1>{featuredPost.title.rendered}</h1>
              <h2>{featuredPost.acf.subheadline}</h2>
              <h4 className="author">by {featuredPost.acf.author}</h4>
            </div>
            <SVGarrow />
          </div>
        </Hero>
      );
    } else {
      return (
        <Loader
          title="Loading"
          spinnerColor="#ffe264"
          fullHeight={true}
          backgroundColor="#0112d4"
        />
      );
    }
  }
}
