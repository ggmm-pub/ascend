import React, { Component } from "react";
import Head from "./Head";
import axios from "axios";
import _ from "lodash";
import { Hero } from "ggmm-react-lib";
import { Link } from "react-router-dom";
import SectionReviews from "./SectionReviews";
import SectionContact from "./SectionContact";

export default class Services extends Component {
  constructor() {
    super();
    this.state = {
      services: [],
      currentReview: 0
    };
  }

  componentDidMount() {
    const SITE_URL = "https://dashboard.ascendtalentstrategies.com",
      { id } = this.props.match.params;

    axios
      .get(SITE_URL + "/wp-json/wp/v2/services?slug=" + id + "&_embed")
      .then(res => {
        this.setState(
          {
            pageTitle: res.data[0].title.rendered,
            pageContent: res.data[0].content.rendered,
            featuredImage: res.data[0].better_featured_image.source_url,
            bullets: res.data[0].acf.sblock_bull,
            icon: res.data[0].acf.service_icon,
            currentId: res.data[0].id,
            hasReviews: res.data[0].acf.reviews,
            postData: res.data[0].acf,
            seo: res.data[0].yoast_meta
          },

          () => this.getData()
        );
      });
  }

  getData() {
    const SITE_URL = "https://dashboard.ascendtalentstrategies.com";

    axios.get(SITE_URL + "/wp-json/wp/v2/services").then(res => {
      _.map(res.data, (o, index) => {
        const pLength = Object.keys(res.data).length - 1, //6
          currentIndex = this.state.currentId,
          currentPost = _.findIndex(res.data, ["id", currentIndex]);
        if (index === pLength && currentPost === 0) {
          this.setState(
            {
              prevPost: o,
              prevTitle: o.title.rendered,
              prevSlug: o.slug
            },
            () => window.scrollTo(0, 0)
          );
        } else if (index === currentPost + 1) {
          this.setState(
            {
              nextPost: o,
              nextTitle: o.title.rendered,
              nextSlug: o.slug
            },
            () => window.scrollTo(0, 0)
          );
        } else if (index === currentPost - 1 && currentPost !== 0) {
          this.setState(
            {
              prevPost: o,
              prevTitle: o.title.rendered,
              prevSlug: o.slug
            },
            () => window.scrollTo(0, 0)
          );
        } else if (index === 0 && currentPost === pLength) {
          this.setState(
            {
              nextPost: o,
              nextTitle: o.title.rendered,
              nextSlug: o.slug
            },
            () => window.scrollTo(0, 0)
          );
        }
      });
    });
  }

  renderPostNav() {
    const {
      prevPost,
      nextPost,
      prevSlug,
      nextSlug,
      prevTitle,
      nextTitle
    } = this.state;

    if (prevPost || nextPost) {
      return (
        <div className="post-nav">
          <div className="button-container">
            {prevPost && (
              <Link className="small-button b-bef" to={"/services/" + prevSlug}>
                {prevTitle}
              </Link>
            )}
            {nextPost && (
              <Link className="small-button b-af" to={"/services/" + nextSlug}>
                {nextTitle}
              </Link>
            )}
          </div>
        </div>
      );
    }
  }

  renderHead() {
    const { seo, featuredImage } = this.state,
      url = window.location.href;

    if (seo) {
      return (
        <Head
          ogImage={featuredImage}
          pageTitle={seo.yoast_wpseo_title}
          url={url}
          des={seo.yoast_wpseo_metadesc}
        />
      );
    }
  }

  render() {
    const {
      pageTitle,
      pageContent,
      featuredImage,
      bullets,
      icon,
      currentReview,
      postData,
      hasReviews
    } = this.state;
    return (
      <div>
        {this.renderHead()}
        <div className="hero">
          <Hero
            type="image"
            imageUrl={featuredImage}
            height="40"
            minHeight="250"
            bgPosition="center center"
            customClass="service-header"
          />
        </div>
        <div className="service-content">
          <img className="service-icon" src={icon} alt={pageTitle} />
          <h1 className="page-title green-back">{pageTitle}</h1>
          <div
            className="content s-cont"
            dangerouslySetInnerHTML={{ __html: pageContent }}
          />
        </div>
        <div className="service-bottom">
          <div className="service-bullets">
            <h2>services</h2>
            <ul>
              {_.map(bullets, bullets => {
                return <li>{bullets.subservices}</li>;
              })}
            </ul>
          </div>
          {this.renderPostNav()}
          <div className="service-reviews">
            {hasReviews && (
              <SectionReviews currentReview={currentReview} data={postData} />
            )}
          </div>
        </div>
        <div className="contact-wrap">
          <SectionContact />
        </div>
      </div>
    );
  }
}
