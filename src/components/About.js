import React, { Component } from "react";
import Head from "./Head";
import Team from "./Team";
import axios from "axios";
import { Hero } from "ggmm-react-lib";

class About extends Component {
  constructor() {
    super();
    this.state = {
      about: [],
      services: []
    };
  }

  componentDidMount() {
    const SITE_URL = "https://dashboard.ascendtalentstrategies.com";

    axios.get(SITE_URL + "/wp-json/wp/v2/pages?slug=about").then(res => {
      this.setState(
        {
          pageTitle: res.data[0].title.rendered,
          seo: res.data[0].yoast_meta,
          pageContent: res.data[0].content.rendered,
          sectionTitle: res.data[0].acf.team_section_headline,
          featuredImage: res.data[0].better_featured_image.source_url
        },
        () => window.scrollTo(0, 0)
      );
    });
  }

  renderHead() {
    const { seo, featuredImage } = this.state,
      url = window.location.href;

    if (seo) {
      return (
        <Head
          ogImage={featuredImage}
          pageTitle={seo.yoast_wpseo_title}
          url={url}
          des={seo.yoast_wpseo_metadesc}
        />
      );
    }
  }

  render() {
    const { pageTitle, pageContent, sectionTitle, featuredImage } = this.state;
    return (
      <div>
        {this.renderHead()}
        <div className="hero">
          <Hero
            type="image"
            imageUrl={featuredImage}
            height="40"
            minHeight="250"
            customClass="about-header"
            bgPosition="center center"
          />
        </div>
        <div className="about-content">
          <h1 className="page-title blue-back">{pageTitle}</h1>
          <div
            className="content a-cont"
            dangerouslySetInnerHTML={{ __html: pageContent }}
          />
        </div>
        <Team sectionTitle={sectionTitle} />
      </div>
    );
  }
}

export default About;
