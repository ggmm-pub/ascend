import React, { Component } from "react";
import _ from "lodash";
import SVGquote from "./SVGquote";
import { CSSTransition, TransitionGroup } from "react-transition-group";

const styles = [
  {
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [
          {
              "color": "#e9e9e9"
          },
          {
              "lightness": 17
          }
      ]
  },
  {
      "featureType": "landscape",
      "elementType": "geometry",
      "stylers": [
          {
              "color": "#f5f5f5"
          },
          {
              "lightness": 20
          }
      ]
  },
  {
      "featureType": "road.highway",
      "elementType": "geometry.fill",
      "stylers": [
          {
              "color": "#ffffff"
          },
          {
              "lightness": 17
          }
      ]
  },
  {
      "featureType": "road.highway",
      "elementType": "geometry.stroke",
      "stylers": [
          {
              "color": "#ffffff"
          },
          {
              "lightness": 29
          },
          {
              "weight": 0.2
          }
      ]
  },
  {
      "featureType": "road.arterial",
      "elementType": "geometry",
      "stylers": [
          {
              "color": "#ffffff"
          },
          {
              "lightness": 18
          }
      ]
  },
  {
      "featureType": "road.local",
      "elementType": "geometry",
      "stylers": [
          {
              "color": "#ffffff"
          },
          {
              "lightness": 16
          }
      ]
  },
  {
      "featureType": "poi",
      "elementType": "geometry",
      "stylers": [
          {
              "color": "#f5f5f5"
          },
          {
              "lightness": 21
          }
      ]
  },
  {
      "featureType": "poi.park",
      "elementType": "geometry",
      "stylers": [
          {
              "color": "#dedede"
          },
          {
              "lightness": 21
          }
      ]
  },
  {
      "elementType": "labels.text.stroke",
      "stylers": [
          {
              "visibility": "on"
          },
          {
              "color": "#ffffff"
          },
          {
              "lightness": 16
          }
      ]
  },
  {
      "elementType": "labels.text.fill",
      "stylers": [
          {
              "saturation": 36
          },
          {
              "color": "#333333"
          },
          {
              "lightness": 40
          }
      ]
  },
  {
      "elementType": "labels.icon",
      "stylers": [
          {
              "visibility": "off"
          }
      ]
  },
  {
      "featureType": "transit",
      "elementType": "geometry",
      "stylers": [
          {
              "color": "#f2f2f2"
          },
          {
              "lightness": 19
          }
      ]
  },
  {
      "featureType": "administrative",
      "elementType": "geometry.fill",
      "stylers": [
          {
              "color": "#fefefe"
          },
          {
              "lightness": 20
          }
      ]
  },
  {
      "featureType": "administrative",
      "elementType": "geometry.stroke",
      "stylers": [
          {
              "color": "#fefefe"
          },
          {
              "lightness": 17
          },
          {
              "weight": 1.2
          }
      ]
  }
]

class SectionReviews extends Component {
  constructor() {
    super();
    this.state = {
      currentReview: 0
    };
  }

  renderMarkup = content => {
    return { __html: content };
  };

  renderReviews() {
    const { data } = this.props,
      { currentReview } = this.state;
    if (data) {
      return _.map(data.reviews, (o, i) => {
        if (i === currentReview) {
          return (
            <CSSTransition key={i} timeout={500} classNames="fade">
              <div className="review-flex">
                <div className="left">
                  <SVGquote />
                  <div
                    className="quote-headshot"
                    style={{
                      backgroundImage: "url(" + o.review_headshot + ")"
                    }}
                    alt={o.review_name}
                  />
                  <div className="name">
                    <h4>{o.review_name}</h4>
                    <h4>{o.review_company}</h4>
                  </div>
                </div>
                <div className="right">
                  <div dangerouslySetInnerHTML={this.renderMarkup(o.review)} />
                </div>
              </div>
            </CSSTransition>
          );
        }
      });
    }
  }

  renderDots() {
    const { data } = this.props,
      { currentReview } = this.state;

    if (data) {
      return _.map(data.reviews, (o, i) => {
        if (i === currentReview) {
          return <div className="dot active" />;
        } else {
          return <div onClick={this.handleReviewChange(i)} className="dot" />;
        }
      });
    }
  }

  renderArrows() {
    const { data } = this.props,
      { currentReview } = this.state;
    if (data) {
      return _.map(data.reviews, (o, i) => {
        if (i === currentReview) {
          return <i class="fas fa-chevron-double-left" />;
        } else {
          return <div onClick={this.handleReviewChange(i)} className="dot" />;
        }
      });
    }
  }

  handleReviewChange = i => e => {
    this.setState({
      currentReview: i
    });
  };

  handleArrow = dir => e => {
    const { data } = this.props,
      { currentReview } = this.state;

    if (data) {
      const reviewLength = Object.keys(data.reviews).length - 1; //1

      if (currentReview === 0 && dir === "left") {
        this.setState({
          currentReview: reviewLength
        });
      }

      if (currentReview === reviewLength && dir === "right") {
        this.setState({
          currentReview: 0
        });
      } else if (dir === "right") {
        this.setState({
          currentReview: currentReview + 1
        });
      } else if (dir === "left" && currentReview !== 0) {
        this.setState({
          currentReview: currentReview - 1
        });
      }
    }
  };

  render() {
    const { data } = this.props,
      length = Object.keys(data.reviews).length;

    return (
      <div className="reviews">
        <div className="review-block">
          <TransitionGroup>{this.renderReviews()}</TransitionGroup>
          {length > 1 && (
            <div>
              <div className="dot-container">{this.renderDots()}</div>
              <div className="arrow-container">
                <i
                  className="fas fa-chevron-double-left"
                  onClick={this.handleArrow("left")}
                />
                <i
                  className="fas fa-chevron-double-right"
                  onClick={this.handleArrow("right")}
                />
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default SectionReviews;
