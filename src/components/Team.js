import React, { Component } from "react";
import axios from "axios";
import _ from "lodash";
import "../polyfills";
import styled from "styled-components";

export default class Team extends Component {
  constructor() {
    super();
    this.myRef = React.createRef();
    this.state = {
      team: [],
      currentlySelected: 103
    };
  }
  componentDidMount() {
    const SITE_URL = "https://dashboard.ascendtalentstrategies.com";

    axios.get(SITE_URL + "/wp-json/wp/v2/team").then(res => {
      this.setState({
        team: res.data
      });
    });
  }

  renderCurrentLineup() {
    const team = [...this.state.team].reverse();
    console.log(team);
    console.log(this.state.team);
    if (this.state.team) {
      return _.map(team, (o, i) => {
        return (
          <div className="team-member" onClick={this.handleMemberChange(o.id)}>
            <img
              src={o.better_featured_image.source_url}
              alt={o.title.rendered}
            />
            <h4>{o.title.rendered}</h4>
          </div>
        );
      });
    }
  }

  renderCurrentBio() {
    const { currentlySelected, team } = this.state;
    return _.map(team, (o, i) => {
      if (o.id === currentlySelected) {
        return (
          <div className="current-bio-member">
            <div className="bio-c">
              <div className="current-headshot">
                <img
                  src={o.better_featured_image.source_url}
                  alt={o.title.rendered}
                />
              </div>
              <div className="current-member-title">
                <h1>{o.title.rendered}</h1>
                <h4>{o.acf.title}</h4>
                <a href={o.acf.email_address}>
                  <p>{o.acf.email_address}</p>
                </a>
              </div>
            </div>
            <div
              className="bio"
              dangerouslySetInnerHTML={{ __html: o.content.rendered }}
            />
            <a className="small-button" href="#members">
              <i class="fas fa-angle-double-up" /> our consulting team{" "}
              <i class="fas fa-angle-double-up" />
            </a>
          </div>
        );
      }
    });
  }

  handleMemberChange = i => e => {
    window.scrollTo(0, this.myRef.current.offsetTop);

    this.setState({
      currentlySelected: i
    });
  };
  render() {
    const teamLength = Object.values(this.state.team).length,
      TeamMember = styled.div`
        display: flex;
        justify-content: space-space-between;
        .team-member {
          width: 23%;
        }
        @media screen and (max-width: 768px) {
          max-width: 400px;
          margin: auto;
        }
      `;
    console.log(teamLength);
    return (
      <div className="team" id="members">
        <div className="head-wrap">
          <div className="team-header">
            <h1>{this.props.sectionTitle}</h1>
          </div>
        </div>
        <div className="bio-top" ref={this.myRef} />
        <div className="team-wrap">
          <TeamMember>{this.renderCurrentLineup()}</TeamMember>
          <div className="current-bio">{this.renderCurrentBio()}</div>
        </div>
      </div>
    );
  }
}
